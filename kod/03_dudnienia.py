from dplib import *
from math import sin, cos, pi
import matplotlib.pyplot as plt

frequency1 = 440  # częstotliwość pierwszego dzwięku wyrażona w Hz
delta_frequency = 1  # różnica częstotliwości wyrażona w Hz
sample_rate = 44100  # próbkowanie - ilość próbek dzwięku zapisywanych na sekundę
length = 3  # długość czasu przez jaki będziemy otwarzać dzwięk


audio_buf = []
time_buf = []


frequency2 = frequency1 + delta_frequency  # oblicznie drugiej częstotliwości (441Hz)


for i in range(round(length*sample_rate)):
	t = i/sample_rate  
	v = sin(2*pi*frequency1*t)+sin(2*pi*frequency2*t)  # oczekiwana wartość funkcji to suma dwóch sinów o różnych częstotliwościach
	v *= 0.5  # poniważ funkcja play przyjmuje listę wartości w przedziale <-1, 1>, zaś suma dwóch sinusów mieści się w przedziale <-2, 2> dzielimy ją przez 2
	
	time_buf.append(t)
	audio_buf.append(v)

play(audio_buf, sample_rate)


# spróbujemy potwierdzić otrzymaną nazajęciach zależność,
# że przy małej różnicy częstotliwości delta_frequency
# suma sinusów odpowieada iloczynowi cosinusa i sinusa o poniższych częstotiwościach
cos_frequency = delta_frequency/2
sin_frequency = frequency1

theory_audio_buf = []  # tworzymy nowy bufor

# moglibyśmy od nowa przliczać numer próbki na sekundę, ale wykorzystane już obliczone wartości z time_buf
for t in time_buf:
	v = 2 * cos(2*pi*cos_frequency*t) * sin(2*pi*sin_frequency*t)
	v *= 0.5
	
	theory_audio_buf.append(v)

# odtwarzamy i słuchamy czy rzeczywiście otrzymaliśmy to samo
play(theory_audio_buf, sample_rate)



#narysujmy jeszcze wykres

# wyświelimy tylko pierwszy okres cosinusa
max_index = round(0.5/cos_frequency*sample_rate)

plt.plot(time_buf[:max_index], audio_buf[:max_index], label="sin+sin")  # rysowanie serii danych dla pierwszego bufora
plt.plot(time_buf[:max_index], theory_audio_buf[:max_index], label="cos*sin")  # seria danych dla drugiego bufora
plt.legend()  # włączenie wyświetlania legendy
plt.show()

# Widzimy że przebiegu z grubsza się z gadzają.

# Ponieważ użyliśmy przybliżenia przy użyciu lupy można zauważyć, że na początku wykresu przebiegi są niemal zupełnie zgodne,
# natomiast na jego końcu nieco się rozchodzą.

# efekt ten nie nastąpi jeżeli zmienimy metodę liczenia częstotliwości sinusa stosowania przybliżeń, to jest:
sin_frequency = (frequency1+frequency2)/2 
# czyli
sin_frequency = frequency1+delta_frequency/2
