from dplib import *  # imporutemy fszystko z modułu dplib
from math import sin, pi  # importujemy funkcję sin i zmienną pi z modułu math
import matplotlib.pyplot as plt  # importujemy moduł pyplot z modułu matplotlib nazywany jako plt

frequency = 440  # częstotliwość generowanego dzwięku wyrażona w Hz
sample_rate = 44100  # próbkowanie - ilość próbek dzwięku zapisywanych na sekundę
length = 2  # długość czasu przez jaki będziemy otwarzać dzwięk

audio_buf = []  # bufor przechowujący wartości dzwięku
time_buf = []  # bufor przechowujący czas ( w sekundach ) odowiadający próbkom wykorzystany na osi x wykresu ( wartość audio_buf[n] jest otwarzana w sekundzie time_buf[n] )

# pętla for, zmienna i odpowiada numerowi próbki których jest length*sample_rate.
# Ponieważ ilość próbek musi być liczbą całkowitą wartość ta jest zaokrąglana) 
for i in range(round(length*sample_rate)):
	t = i/sample_rate  # czas w sekundach odpowiadający danej próbce obliczny jako numer próbki podzielony przez próbkowanie
	v = sin(2*pi*frequency*t)  # wartość jaką przyjmuje funkcja dzwięku (sinus) w momencie t
	
	# dodanie wartości do buforów
	time_buf.append(t)
	audio_buf.append(v)

# odtworznie dzwięku przy użyciu funkcji zdefiniowanej w pliku dplib.py
play(audio_buf, sample_rate)

# narysowanie wykresu na podstawie całych buforów 
# ponieważ okres wynoszący około 0.002s << czasu odtwarzania 2s nie widać przebiegu sinusa
# można użyć lupy do przybliżenia widoku
plt.plot(time_buf, audio_buf)
plt.show()

plt.cla()  # wyczyszczenie wykresu


# spróbujemy narysować tylko 3 pierwsze okresy 

T = 1/frequency  # obliczamy okres jako odwrotność częstotliwości
max_time = 3*T  # obliczamy górną graniczę wyświetlanego czasu jako trzykrotność okresu
max_index = round(max_time*sample_rate)  # obliczamy odpowiadający tej granicy index w buforze dzieląc czas w sekundach przez próbkowanie

# max_index = round(3/frequency*sample_rate)  # trzy powyższe operacje można zapisać w jednej linii

# rysowanie
plt.plot(time_buf[:max_index], audio_buf[:max_index])
# dodatkwo zdefiniujemy zmienną zawierającą tytuł dla wykresu
title = "Czas: {}s - Próbek {}".format(max_time, max_index) 
plt.title(title)  # ustawienie tytułu jako zawartości zmiennej
plt.show()
