import wave
import struct
import tempfile
import os
from playsound import playsound


def save_wav(audio_buffer: list, file_name: str, sample_rate) -> None:
    print("Sound generation started")

    # Open up a wav file
    wav_file=wave.open(file_name,"w")

    # wav params
    nchannels = 1

    sampwidth = 2

    # 44100 is the industry standard sample rate - CD quality.  If you need to
    # save on file size you can adjust it downwards. The stanard for low quality
    # is 8000 or 8kHz.
    nframes = len(audio_buffer)
    comptype = "NONE"
    compname = "not compressed"
    wav_file.setparams((nchannels, sampwidth, sample_rate, nframes, comptype, compname))

    # WAV files here are using short, 16 bit, signed integers for the
    # sample size.  So we multiply the floating point data we have by 32767, the
    # maximum value for a short integer.  NOTE: It is theortically possible to
    # use the floating point -1.0 to 1.0 data directly in a WAV file but not
    # obvious how to do that using the wave module in python.
    for sample in audio_buffer:
        wav_file.writeframes(struct.pack('h', int( sample * 32767.0 )))

    wav_file.close()

    print("Sound generation finished")

    return

def play(audio_buffer: list, sample_rate) -> None:
    tmp_file = "tmp.wav"
    save_wav(audio_buffer, tmp_file, sample_rate)
    print("Play started")
    playsound(tmp_file)
    print("Play finished")

    return
