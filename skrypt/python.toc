\babel@toc {polish}{}
\contentsline {chapter}{\numberline {1}Wstęp}{2}{chapter.1}%
\contentsline {section}{\numberline {1.1}Rodzaje języków programowania}{2}{section.1.1}%
\contentsline {chapter}{\numberline {2}Instalacja komponentów}{4}{chapter.2}%
\contentsline {section}{\numberline {2.1}Interpreter Python}{4}{section.2.1}%
\contentsline {subsection}{\numberline {2.1.1}Środowisko Geany}{4}{subsection.2.1.1}%
\contentsline {section}{\numberline {2.2}Zależności}{7}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}pip}{7}{subsection.2.2.1}%
\contentsline {subsection}{\numberline {2.2.2}ipython}{7}{subsection.2.2.2}%
\contentsline {subsection}{\numberline {2.2.3}Proste zależności - playsound}{8}{subsection.2.2.3}%
\contentsline {subsection}{\numberline {2.2.4}Zależności wymagające kompilacji - matplotlib}{8}{subsection.2.2.4}%
