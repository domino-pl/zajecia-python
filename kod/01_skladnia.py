# Komentarz jednoliniowy

""" Komentarz
wielolinowy """

'''
Inny
styl
komentarza
wieloliniowego
'''

# poniższe komendy najepiej przetestować w interaktywnej powłoce
# python.exe lub ipython

print("Hello world")  # wypisanie na ekran napisu "Hello world"
2  # wartość 2

a = 5  # nadanie zmiennej a wartości 5


""" ------------ operacje artmetyczne ------------ """

a + 2
a - 2
a * 2
a / 2
a % 2  # reszta z dzielenia
a ** 2  # potęgowanie(takrze pierwiastowanie wykładnikiem ułamkowym)
a // 2  # dzielenie całkowito liczbowe

# wykonywanie operacji artmetycznych na zmiennej z przypisanem do niej wyniku aperacji
a += 2
a -= 2
a *= 2
a /= 2
a %= 2  # reszta z dzielenia
a **= 2  # potęgowanie(takrze pierwiastowanie wykładnikiem ułamkowym)
a //= 2  # dzielenie całkowito liczbowe

# nie ma post/pre in/dekrementacji

b = a + 2 # nadanie zmiennej wartości zwróconej w wyniku operacji dodawania

""" ------------ listy ------------ """

l = []  # utowrzenie pustej listy
l = [1, 2, 3, 5]  # utworzenie listy zawierającej trzy elementy
l.append(4)  # dodatnie elementu na końcu listy
del l[1] # usunięcie drugiego elementu listy
# poniższe instrukcjie nie modyfikują instniejącej listy tylko na jej podstawie zwracają jakąś wartość
l[0]  # wybranie pierwszego elementu listy
l[-1]  # wybranie ostatniego elementu listy
l[1:3]  # wybranie podlisty składającej się z drugiego i trzeciego elementu listy
l.sort()  # sortowanie listy
len(l)  # długość listy

# zagadnienie do zbadania we własnym to inne rodzaje kolekcji
# https://riptutorial.com/pl/python/example/4239/rodzaje-kolekcji
# krotki(tuple), słowniki, sety

""" ------------ napisy ------------ """
s = "napis"  # nadanie zmiennej s wartości "napis"

# napisy są niemutowalne czyli nie modyfikowalne - oznacza to, że zawsze gdy coś w nich zmieniamy
# tak naprawdę nie modyfikujemy zawartości zmiennej tylko nadajemy jej nową wartość

len(s)  # długość napisu

s.capitalize()  # pierwsza litera wielka
s.upper()  # wszytkie litery wielkie

", ".join(["1", "2", "3", "4"])  # połącznie elementów listy zawierającej napisy łącznikiem ", "
"to.będzie.lista".split(".")  # utworzenie listy z napisy na podstawie separatora "."

s[0]  # operatory wyłuskania działają tak samo jak dla list - tutaj wybranie pierwszej litery

s = "a\nb" # napis zawierający 2 linie
print(s)

s = "a\\nb" # użycie "\n" jako zwykłych dwóch znaków
print(s)

s = r"a\nb" # jak wyżej
print(s)


""" ------------ typy danych ------------ """

type(1)  # wartość 1 jest typu całkowitoliczbowego
a = 1
type(a)  # a także
a = 2.2
type(a)  # liczba zmienno przecinkowa
type([])  # lista
type({})  # słownik
type(())  # krotka

a = (1)
type(a)  # liczba! - nie zadziała a[0]
a = (1,)
type(a)  # krotka! - zadziała a[0]

# rzutowanie typów
a = str(2.2)
type(a)  # napis "2.2"

#"a = " + 2.2  # błąd - nie można łączyć napisu u liczby
"a = " + str(2.2)  # wporządku

#", ".join(["1", 2, "3", "4"])  # taki sam błąd

# sposoby zamiany float na int
a = 2.6
int(a)
round(a)

""" ------------ formatowanie napisów ------------ """
a = 1
b = 2.0

print("a =", a, "b=", b)

s = "a={}, b={}".format(a, b) # w internecie można znaleźć dodatkowe parametry 
# wstawiane w klamry odpowadające np. za ilość miejsc po przecinku itp.
print(s)


""" ------------ wyrażenia logiczne ------------ """
# zwaracają True albo False

a = 4

a == 3
a != 3
a > 3
a < 3
a >= 3
a <= 4

3 < a < 4

3 < a and a < 4

a < 4 or a > 4

1 in [1, 2, 3, 4]  # czy 1 jest elementem listy
"a" in {"a": 1, "b": 2}  # czy w słowniku jest element o kluczu "a"
1 in {"a": 1, "b": 2}.values()  # czy w słowniku jest element o warości 1


""" ------------ instrukcja warunkowa ------------ """
if a > 0:
	print("a większe od zera")
elif a < 0:
	print("a mniejsze od zera")
#członów elfi może być więcej
else:
	print("a równe 0")



# instrukcję warunkową można zapisać w jednej lini:
napis = "a jest większe od zera" if a > 0 else "a jest nie większe od zera"

""" ------------ pętle ------------ """

i = 0
while i < 5:
	print("i = {}".format(i))
	i += 1

print( list( range(10) ) ) # zakres 0,9
print( list( range(2,10) ) ) # zakres 2,9
print( list( range(2,10,2) ) ) # zakres 2,9 ze skokiem 2

for i in range(10):
	print("i = {}".format(i))

for element in ["a", "b", 12, 2.2]:
	print("element = {}".format(element))

sl = {"k1": "v1", "k2": 2}
print(sl.items())
for k, v in sl.items():
	print("klucz={}, wartość={}".format(k,v))
	
# for może być zapisany w jednej linii
# generowanie listy kwadratów:
l = [i**2 for i in range(2, 11)]
print(l)



